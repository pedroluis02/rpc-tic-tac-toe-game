
# RPC Tic Tac Toe
A Tic Tac Toe game sample using RPC (Remote Procedure Call) on SO Linux


#### Install RPC
```
sudo apt-get install rpcbind
```

#### Compile
```
make -f Makefile.triqui
```

#### Run server
```
./triqui_server
```

#### Run client
```
./triqui_client <IP_HOST>
```
