/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "triqui.h"

enum tipo_mesaje{TU_TURNO, NO_TURNO, TU_NUEVO, OTRO_NUEVO, POSICION_OCUPADA, CONECTADO, OTRO_DES,
			TU_DES, GANADOR, PERDEDOR, EMPATE};

static Juego juego = {1, NULL, NULL, FALSE};
static int mensaje1 = -1, mensaje2 = -1;
static int matrizTriqui[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
static int posicionesLibres = 9, posJugada1 = -1, posJugada2 = -1;

void
reiniciarTablero()
{
	int i=0, j=0;
	for(i=0; i<3; i++)
	   for(j=0; j<3; j++)
		   matrizTriqui[i][j] = 0;
	posicionesLibres = 9;
	posJugada1 = -1; posJugada2 = -1;
	mensaje1 = -1, mensaje2 = -1;
}
int
getFila(int pos)
{
    if(pos==1 || pos==2 || pos==3)
       return 0;
    else if(pos==4 || pos==5 || pos==6)
       return 1;
    else if(pos==7 || pos==8 || pos==9)
       return 2;
    else
       return -1;
}
int
getColumna(int pos)
{
    if(pos==1 || pos==4 || pos==7)
       return 0;
    else if(pos==2 || pos==5 || pos==8)
       return 1;
    else if(pos==3 || pos==6 || pos==9)
       return 2;
    else
       return -1;
}
bool_t
isPosicionVacia(int pos)
{
    int row = getFila(pos);
    int column = getColumna(pos);

    if(row==-1 || column==-1)
        return FALSE;
    if(matrizTriqui[row][column]==0)
       return TRUE;
    else
       return FALSE;
}
void
colocarFicha(int numeroJugador, int pos)
{
	int fila = getFila(pos);
	int column = getColumna(pos);

	matrizTriqui[fila][column] = numeroJugador;
	posicionesLibres = posicionesLibres - 1;
	if(numeroJugador==1)
	   posJugada1 = pos;
	else
	   posJugada2 = pos;
	fprintf(stderr, "%s%i%s%i%s\n", "Jugador [", numeroJugador,"]-Pos[", pos,"]");
}
bool_t
isJuegoCompleto(int number)
{
        //* Comprueba las lineas horizontales */
        if(matrizTriqui[0][0] == number && matrizTriqui[0][0] == matrizTriqui[0][1] && matrizTriqui[0][0] == matrizTriqui[0][2])
            return TRUE;
        else if(matrizTriqui[1][0] == number && matrizTriqui[1][0] == matrizTriqui[1][1] && matrizTriqui[1][0] == matrizTriqui[1][2])
            return TRUE;
        else if(matrizTriqui[2][0] == number && matrizTriqui[2][0] == matrizTriqui[2][1] && matrizTriqui[2][0] == matrizTriqui[2][2])
            return TRUE;
        //* Comprueba las lineas verticales */
        else if(matrizTriqui[0][0] == number && matrizTriqui[0][0] == matrizTriqui[1][0] && matrizTriqui[0][0] == matrizTriqui[2][0])
            return TRUE;
        else if(matrizTriqui[0][1] == number && matrizTriqui[0][1] == matrizTriqui[1][1] && matrizTriqui[0][1] == matrizTriqui[2][1])
            return TRUE;
        else if(matrizTriqui[0][2] == number && matrizTriqui[0][2] == matrizTriqui[1][2] && matrizTriqui[0][2] == matrizTriqui[2][2])
            return TRUE;
        //* Comprueba las lineas diagonales */
        else if(matrizTriqui[0][0] == number && matrizTriqui[0][0] == matrizTriqui[1][1] && matrizTriqui[0][0] == matrizTriqui[2][2])
            return TRUE;
        else if(matrizTriqui[2][0] == number && matrizTriqui[2][0] == matrizTriqui[1][1] && matrizTriqui[2][0] == matrizTriqui[0][2])
            return TRUE;
        else
            return FALSE;
}

/*-----------------------------------------------------------------------------------------------------------------------------------*/

Jugador *
conectar_1_svc(Jugador *argp, struct svc_req *rqstp)
{
	/*
	 * insert server code here
	 */
	juego.activo = TRUE;
	if(juego.jugador1==NULL)
	{
	   static Jugador result1;
	   strcpy(result1.nombreJugador, "Jugador 1");
	   result1.numeroJugador = 1;
	   result1.numeroJuego = juego.numeroJuego;
	   result1.ficha = 'x';
	   result1.turno = TRUE;
	   mensaje1 = TU_TURNO;
	   mensaje2 = NO_TURNO;
	   juego.jugador1 = &result1;
	   fprintf(stderr, "%s\n", "Jugador 1 se conecto...");
	   return &result1;
	}
	else if(juego.jugador2==NULL)
	{
		static Jugador result2;
		strcpy(result2.nombreJugador, "Jugador 2");
		result2.numeroJugador = 2;
		result2.numeroJuego = juego.numeroJuego;
		result2.ficha = '0';
		if(juego.jugador1->turno==TRUE)
		{
		   result2.turno = FALSE;
		   mensaje1 = TU_TURNO;
		   mensaje2 = NO_TURNO;
		}
		else
		{
		   result2.turno = TRUE;
		   mensaje2 = TU_TURNO;
		   mensaje1 = NO_TURNO;
		}
		juego.jugador2 = &result2;
		fprintf(stderr, "%s\n", "Jugador 2 se conecto...");
	    return &result2;
	}
}

Jugador *
nuevojuego_1_svc(Jugador *argp, struct svc_req *rqstp)
{
	/*
	 * insert server code here
	 */
	reiniciarTablero();
	if(argp->numeroJugador==1)
	{
		   static Jugador result1;
		   strcpy(result1.nombreJugador, "Jugador 1");
		   result1.numeroJugador = 1;
		   result1.numeroJuego = juego.numeroJuego;
		   result1.ficha = 'x';
		   result1.turno = TRUE;
		   juego.jugador2->turno = FALSE;
		   mensaje1 = TU_TURNO;
		   mensaje2 = OTRO_NUEVO;
		   juego.jugador1 = &result1;
		   fprintf(stderr, "%s\n", "Jugador 1 inicio nuevo juego...");
		   return &result1;
	}
	else if(argp->numeroJugador==2)
	{
			static Jugador result2;
			strcpy(result2.nombreJugador, "Jugador 2");
			result2.numeroJugador = 2;
			result2.numeroJuego = juego.numeroJuego;
			result2.ficha = '0';
			result2.turno = TRUE;
			mensaje2 = TU_TURNO;
			mensaje1 = OTRO_NUEVO;
			juego.jugador2 = &result2;
			fprintf(stderr, "%s", "Jugador 2 inicio nuevo juego...");
		    return &result2;
	}
}

Mensaje *
esmiturno_1_svc(Jugador *argp, struct svc_req *rqstp)
{

	/*
	 * insert server code here
	 */
	if(argp->numeroJugador==1)
	{
	   static Mensaje  result1;
	   result1.turno = juego.jugador1->turno;
	   result1.jugadaPos = posJugada2;
	   result1.juegoActivo = juego.activo;
	   result1.tipoMensaje = mensaje1;
	   return &result1;
	}
	else if(argp->numeroJugador==2)
	{
	   static Mensaje  result2;
	   result2.turno = juego.jugador2->turno;
	   result2.jugadaPos = posJugada1;
	   result2.juegoActivo = juego.activo;
	   result2.tipoMensaje = mensaje2;
	   return &result2;
	}
}

Jugador *
jugar_1_svc(Jugada *argp, struct svc_req *rqstp)
{
	/*
	 * insert server code here
	 */
	Jugador result;
	int num = argp->jugador->numeroJugador;
	if(isPosicionVacia(argp->posicion)!=TRUE)
	{
	   if(num==1)
	      mensaje1 = POSICION_OCUPADA;
	   else
		  mensaje2 = POSICION_OCUPADA;
	   return NULL;
	}
	if(num==1)
	{
	   juego.jugador1->turno = FALSE;
	   juego.jugador2->turno = TRUE;
	   mensaje2 = TU_TURNO;
	   mensaje1 = NO_TURNO;
	   result = *juego.jugador1;
	}
	else if(num==2)
	{
		juego.jugador1->turno = TRUE;
		juego.jugador2->turno = FALSE;
		mensaje1 = TU_TURNO;
		mensaje2 = NO_TURNO;
		result = *juego.jugador2;
	}
	colocarFicha(num, argp->posicion);
	if(isJuegoCompleto(num)==TRUE)
	{
		if(result.numeroJugador==1)
		{
			   mensaje1 = GANADOR;
			   mensaje2 = PERDEDOR;
		}
		else if(result.numeroJugador==2)
		{
			mensaje2 = GANADOR;
			mensaje1 = PERDEDOR;
		}
		else if(posicionesLibres==0)
		{
			mensaje1 = EMPATE;
			mensaje2 = EMPATE;
		}
	}
	if(result.numeroJugador==1)
	   return juego.jugador1;
	else
	   return juego.jugador2;
}

Mensaje *
desconectar_1_svc(Jugador *argp, struct svc_req *rqstp)
{
	/*
	 * insert server code here
	 */
	juego.numeroJuego = 1;
	juego.activo = FALSE;
	if(argp->numeroJugador==1)
	{
	   static Mensaje  result1;
	   juego.jugador1 = NULL;
	   mensaje1 = TU_DES;
	   mensaje2 = OTRO_DES;
	   result1.tipoMensaje = mensaje1;
	   result1.jugadaPos = -1;
	   result1.turno = FALSE;
	   result1.juegoActivo = juego.activo;
	   return &result1;
	}
	else
	{
	   static Mensaje  result2;
	   juego.jugador2 = NULL;
	   mensaje2 = TU_DES;
	   mensaje1 = OTRO_DES;
	   result2.tipoMensaje = mensaje1;
	   result2.jugadaPos = -1;
	   result2.turno = FALSE;
	   result2.juegoActivo = juego.activo;
	   return &result2;
	}
}


Mensaje *
leermensaje_1_svc(Jugador *argp, struct svc_req *rqstp)
{
	static Mensaje  result;

	/*
	 * insert server code here
	 */

	return &result;
}
