struct Jugador
{
	char nombreJugador[20];
	int numeroJugador;
	int numeroJuego;
	char ficha;
	bool turno;
};
struct Juego
{
	int numeroJuego;
	struct Jugador *jugador1;
	struct Jugador *jugador2;
	bool activo;
};
struct Jugada
{
	struct Jugador *jugador;
	int posicion;
};
struct Mensaje
{
	int jugadaPos;
	int tipoMensaje;
	char mensajeTexto[80];
	bool turno;
	bool juegoActivo;
};

program TRIQUI
{
	version TRIQUI_VERSION
	{
		Jugador conectar(Jugador) = 1;
		Jugador nuevoJuego(Jugador) = 2;
		Mensaje esMiTurno(Jugador) = 3;
		Jugador jugar(Jugada) = 4;
		Mensaje desconectar(Jugador) = 5;
		Mensaje leerMensaje(Jugador) = 6; 
	} = 1;
} = 1991;

